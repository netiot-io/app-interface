FROM anapsix/alpine-java:8
ADD target/app-basic.jar app-basic.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app-basic.jar
