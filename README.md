# Basic application

The application config and visualization are loaded dynamically in the main UI. 

To load the config, the main UI requests the config json and generates the form based on it.

To load the visualization, the main UI requests the generated js from the backend and displays it. Everything defined for the app`s frontend is packaged into one single js file.

## Backend
In `src/main/java`. Basic spring MS structure. Endpoints in `controllers` package. You can follow the trail from these controllers to see the logic.

### Known paths
#### application configuration
`GET app-basic/configuration?applicationInstanceId=`

Return: string (represents the json config to generate the jsonforms form)

`POST app-basic/configuration/{applicationInstanceId}`

Body
```json
{
  "sensor_limit": 2,
  "sensors": [
    {"NAME": {}}
  ] 
}
```

#### application instance
`GET app-basic/application-instance/{applicationInstanceId}/data?millisFrom=&millisTo=`

Return
```json
{
  "devicesData":[
    {
      "samples": [
        {
          "deviceId": 1,
          "timestamp": 1571217103238,
          "timestampR": 1571217103239,
          "values": ""
        }
      ]     
    }
  ] 
}
```

`GET app-basic/application-instance`

Return: string (the js for the app UI)

`POST app-basic/application-instances/internal`

Body
```json
{
  "name": "",
  "status_type": "enable",
  "config": ""
}
```

`PUT app-basic/application-instances/internal`

Body
```json
{
  "id": 1,
  "name": "",
  "status_type": "enable",
  "config": ""
}
```

`DELETE app-basic/application-instances/internal?applicationInstanceId=`

### Database
Add the initial DB config in `resources/db.migartion/V1__initial.sql`. Keep the same `APPLICATION_INSTANCES`.
Any new changes after the initial commit should be added in the same folder with names like `Vx__SMALL_DESCRIPTION.sql`.

### Feign
Communicates with the `application-registry` microservice to check if a user is allowed to access an app.

`GET application-registry/V1/application-instance/internal/checkPermission?userId=&applicationId=`

Return: boolean

Communicates with the `devices` microservice to get the user's devices and to get a device name.

`GET devices/V1/devices/internal`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "decoder_fields": [""],
    "device_type_id": 2,
    "device_type_name": ""
  }
]
```

`GET devices/V1/devices/internal/{deviceId}`

Return: string

### Kafka
Sends a kafka message whenever a new application config is created. This can be helpful for other microservices
that process app data and depend on the app configuration.

Receives a kafka message whenever a new `average` result is computed. This can be sent from the `app-processing` 
microservice.

## Config parameters
We use [jsonforms](https://jsonforms.io/) to dynamically generate the config form on the frontend. The json to configure is defined in `resources/static/config.json`. Please read the jsonforms docs. There MUST be 3 objects in this config: `schema` (field definition), `uischema` (ui field visualization) and `data` (initial data).

## Frontend
In `src\main\frontend`. 
Install with `npm install`.

DO NOT MODIFY `index.js`. The app UI is loaded dynamically in the main UI and it's based on some predefined ids and script attributes.

Use `.env` for any app parameters.

To test the app visualization edit `src\main\frontend\public\index.html` and define the proper `appConfig` in the `tempApp` script.<br> 
Then follow the instructions in the `update` function from `src\main\frontend\src\HotelAppDashboard.js`.<br>
Make sure that the corresponding ids match between what is defined in `index.html` and the test data in the `update` function.<br>
Then run `npm start` in `src\main\frontend`.

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry.

## Tests
Groovy tests in `src/test/groovy`.