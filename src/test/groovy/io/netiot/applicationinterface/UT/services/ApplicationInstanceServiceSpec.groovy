package io.netiot.applicationinterface.UT.services

import io.netiot.applicationinterface.exceptions.ApplicationInstanceException
import io.netiot.applicationinterface.feign.IOServerClient
import io.netiot.applicationinterface.repositories.ApplicationInstanceRepository
import io.netiot.applicationinterface.repositories.DeviceConfigRepository
import io.netiot.applicationinterface.services.ApplicationInstanceService
import io.netiot.applicationinterface.services.DevicesService
import io.netiot.applicationinterface.services.FileService
import io.netiot.applicationinterface.services.KafkaMessageSenderService
import io.netiot.applicationinterface.utils.AuthenticatedUserInfo
import io.netiot.applicationinterface.validators.ApplicationInstanceValidator
import spock.lang.Specification

import static io.netiot.applicationinterface.generators.ApplicationInstanceGenerator.aApplicationInstance
import static io.netiot.applicationinterface.generators.ApplicationInstanceGenerator.aApplicationInstanceModel

class ApplicationInstanceServiceSpec extends Specification {

    def applicationInstanceRepository = Mock(ApplicationInstanceRepository)
    def applicationInstanceValidator = Mock(ApplicationInstanceValidator)
    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def kafkaMessageSenderService = Mock(KafkaMessageSenderService)
    def deviceConfigRepository = Mock(DeviceConfigRepository)
    def devicesService = Mock(DevicesService)
    def fileService = Mock(FileService)
    def ioServerClient = Mock(IOServerClient)

    def applicationInstanceService = new ApplicationInstanceService(applicationInstanceRepository, applicationInstanceValidator,
            authenticatedUserInfo, deviceConfigRepository, kafkaMessageSenderService,  devicesService, fileService, ioServerClient)

    def 'createApplicationInstance'() {
        given:
        def applicationInstanceModel = aApplicationInstanceModel()
        def applicationInstance = aApplicationInstance(id: null)

        when:
        applicationInstanceService.createApplicationInstance(applicationInstanceModel)

        then:
        1 * applicationInstanceRepository.findByApplicationId(applicationInstanceModel.getId()) >> Optional.empty()
        1 * applicationInstanceRepository.save(applicationInstance)
        0 * _
    }

    def 'createApplicationInstance - thrown ApplicationInstanceException'() {
        given:
        def applicationInstanceModel = aApplicationInstanceModel()
        def applicationInstance = aApplicationInstance()

        when:
        applicationInstanceService.createApplicationInstance(applicationInstanceModel)

        then:
        1 * applicationInstanceRepository.findByApplicationId(applicationInstanceModel.getId()) >> Optional.of(applicationInstance)
        0 * _
        thrown(ApplicationInstanceException)
    }

    def 'updateApplicationInstance'() {
        given:
        def applicationInstanceModel = aApplicationInstanceModel(name: "newName")
        def applicationInstanceFromDB = aApplicationInstance(name: "oldName")
        def applicationInstanceSavedInDB = aApplicationInstance(name: "newName")

        when:
        applicationInstanceService.updateApplicationInstance(applicationInstanceModel)

        then:
        1 * applicationInstanceRepository.findByApplicationId(applicationInstanceModel.getId()) >> Optional.of(applicationInstanceFromDB)
        1 * applicationInstanceRepository.save(applicationInstanceSavedInDB)
        0 * _
    }

    def 'updateApplicationInstance - thrown ApplicationInstanceException'() {
        given:
        def applicationInstanceModel = aApplicationInstanceModel()

        when:
        applicationInstanceService.updateApplicationInstance(applicationInstanceModel)

        then:
        1 * applicationInstanceRepository.findByApplicationId(applicationInstanceModel.getId()) >> Optional.empty()
        0 * _
        thrown(ApplicationInstanceException)
    }

    def 'deleteApplicationInstance'() {
        given:
        def applicationInstanceId = 1L
        def applicationInstanceFromDB = aApplicationInstance()

        when:
        applicationInstanceService.deleteApplicationInstance(applicationInstanceId)

        then:
        1 * applicationInstanceRepository.findByApplicationId(applicationInstanceId) >> Optional.of(applicationInstanceFromDB)
        1 * applicationInstanceRepository.deleteByApplicationId(applicationInstanceFromDB.getId())
        1 * deviceConfigRepository.deleteByApplicationInstance_ApplicationId(applicationInstanceId)
        0 * _
    }

    def 'deleteApplicationInstance  - thrown ApplicationInstanceException'() {
        given:
        def applicationInstanceId = 1L

        when:
        applicationInstanceService.deleteApplicationInstance(applicationInstanceId)

        then:
        1 * applicationInstanceRepository.findByApplicationId(applicationInstanceId) >> Optional.empty()
        0 * _
        thrown(ApplicationInstanceException)
    }

}
