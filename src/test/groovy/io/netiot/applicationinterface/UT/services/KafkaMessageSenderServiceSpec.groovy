package io.netiot.applicationinterface.UT.services

import io.netiot.applicationinterface.configurations.KafkaChannels
import io.netiot.applicationinterface.services.KafkaMessageSenderService
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import spock.lang.Specification

import static io.netiot.applicationinterface.generators.ApplicationInstanceEventGenerator.aApplicationInstanceEvent
import static io.netiot.applicationinterface.generators.DeviceRegistrationEventGenerator.aDeviceRegistrationEvent

class KafkaMessageSenderServiceSpec extends Specification {

    def kafkaChannels = Mock(KafkaChannels)
    def messageChannel = Mock(MessageChannel)

    def kafkaMessageSenderService = new KafkaMessageSenderService(kafkaChannels)

    def 'sendApplicationInstanceEvent'(){
        given:
        def applicationInstanceEvent = aApplicationInstanceEvent()

        when:
        kafkaMessageSenderService.sendApplicationInstanceEvent(applicationInstanceEvent)

        then:
        1 * kafkaChannels.applicationBasicInstanceEvents() >> messageChannel
        1 * messageChannel.send( _ as Message) >> true
        0 * _
    }

}
