package io.netiot.applicationinterface.UT.validators

import io.netiot.applicationinterface.entities.Role
import io.netiot.applicationinterface.exceptions.UnauthorizedOperationException
import io.netiot.applicationinterface.services.ApplicationService
import io.netiot.applicationinterface.utils.AuthenticatedUserInfo
import io.netiot.applicationinterface.validators.ApplicationInstanceValidator
import spock.lang.Specification

import static io.netiot.applicationinterface.generators.ApplicationInstanceGenerator.aApplicationInstance

class ApplicationInstanceValidatorSpec extends Specification {

    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def applicationService = Mock(ApplicationService)

    def applicationInstanceValidator = new ApplicationInstanceValidator(authenticatedUserInfo, applicationService)

    def 'validateOnGet'() {
        given:
        def applicationInstance = aApplicationInstance()
        def userId = 1L

        when:
        applicationInstanceValidator.validateOnGet(applicationInstance)

        then:
        1 * authenticatedUserInfo.getRole() >> role
        getUserId * authenticatedUserInfo.getId() >> userId
        getUserId * applicationService.checkPermission(userId, applicationInstance.getApplicationId()) >> true
        0 * _

        where:
        role               | getUserId
        Role.ADMIN         | 0
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    def 'validateOnGet throw UnauthorizedOperationException'() {
        given:
        def applicationInstance = aApplicationInstance()
        def userId = 1L

        when:
        applicationInstanceValidator.validateOnGet(applicationInstance)

        then:
        1 * authenticatedUserInfo.getRole()
        1 * authenticatedUserInfo.getId() >> userId
        1 * applicationService.checkPermission(userId, applicationInstance.getApplicationId()) >> false
        0 * _
        thrown(UnauthorizedOperationException)
    }
}
