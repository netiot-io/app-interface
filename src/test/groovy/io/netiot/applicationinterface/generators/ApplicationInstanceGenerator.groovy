package io.netiot.applicationinterface.generators

import io.netiot.applicationinterface.entities.ApplicationInstance
import io.netiot.applicationinterface.models.ApplicationInstanceModel
import io.netiot.applicationinterface.models.StatusType

class ApplicationInstanceGenerator {

    static aApplicationInstance(Map overrides = [:]) {
        Map values = [
                id: 1L,
                name : "applicationInstanceName",
                applicationId: 1L,
                isConfigured: Boolean.FALSE,
                activated: Boolean.TRUE,
                config: "{}"
        ]
        values << overrides
        return ApplicationInstance.newInstance(values)
    }

    static aApplicationInstanceModel(Map overrides = [:]) {
        Map values = [
                id: aApplicationInstance().getId(),
                name : "applicationInstanceName",
                statusType: StatusType.ENABLE,
        ]
        values << overrides
        return ApplicationInstanceModel.newInstance(values)
    }

}
