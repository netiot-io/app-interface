package io.netiot.applicationinterface.generators

import io.netiot.applicationinterface.models.events.ApplicationInstanceEvent

class ApplicationInstanceEventGenerator {

    static aApplicationInstanceEvent(Map overrides = [:]) {
        Map values = [
                id: 1L,
                deviceId: 1L,
                payloadFields: ["test"]
        ]
        values << overrides
        return ApplicationInstanceEvent.newInstance(values)
    }

}
