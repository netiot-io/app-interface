package io.netiot.applicationinterface.generators

import io.netiot.applicationinterface.models.events.DeviceRegistrationEvent
import io.netiot.applicationinterface.models.events.DeviceRegistrationType

class DeviceRegistrationEventGenerator {

    static aDeviceRegistrationEvent(Map overrides = [:]) {
        Map values = [
                applicationId: 1L,
                deviceId : 1L,
                deviceRegistrationType: DeviceRegistrationType.REGISTER
        ]
        values << overrides
        return DeviceRegistrationEvent.newInstance(values)
    }

}