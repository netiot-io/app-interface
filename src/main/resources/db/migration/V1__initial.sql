CREATE SEQUENCE SEQ_APPLICATION_INSTANCE;

CREATE TABLE APPLICATION_INSTANCES (
  id                      BIGINT                PRIMARY KEY,
  application_id          BIGINT                NOT NULL UNIQUE,
  name                    CHARACTER VARYING(50) NOT NULL,
  configured              BOOLEAN               NOT NULL DEFAULT FALSE,
  activated               BOOLEAN               NOT NULL DEFAULT FALSE,
  config                  TEXT                  DEFAULT '{}'
);

CREATE SEQUENCE SEQ_DEVICE_CONFIG;

CREATE TABLE DEVICE_CONFIG (
  id                      BIGINT PRIMARY KEY,
  application_instance_id BIGINT NOT NULL,
  device_id               BIGINT NOT NULL,
  sensors                 TEXT NOT NULL,
  FOREIGN KEY (application_instance_id) REFERENCES APPLICATION_INSTANCES (id)
);