package io.netiot.applicationinterface;

import io.netiot.applicationinterface.configurations.KafkaChannels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableHystrix
@EnableAsync
@EnableWebMvc
@EnableDiscoveryClient
@EnableBinding(KafkaChannels.class)
@SpringBootApplication
public class ApplicationInterface {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationInterface.class, args);
	}
}
