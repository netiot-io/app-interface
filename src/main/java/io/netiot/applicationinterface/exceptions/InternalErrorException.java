package io.netiot.applicationinterface.exceptions;

public class InternalErrorException extends RuntimeException {

    public InternalErrorException() {
    }

    public InternalErrorException(String message) {
        super(message);
    }

}
