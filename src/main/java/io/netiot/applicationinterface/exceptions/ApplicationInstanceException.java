package io.netiot.applicationinterface.exceptions;

public class ApplicationInstanceException extends RuntimeException {

    public ApplicationInstanceException() {
    }

    public ApplicationInstanceException(String message) {
        super(message);
    }
}
