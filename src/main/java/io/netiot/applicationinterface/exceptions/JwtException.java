package io.netiot.applicationinterface.exceptions;

public class JwtException extends RuntimeException {

    public JwtException() {
    }

    public JwtException(String message) {
        super(message);
    }
}
