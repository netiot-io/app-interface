package io.netiot.applicationinterface.configurations;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface KafkaChannels {

    String AVERAGE_EVENT_INPUT= "average-event-input";
    String APPLICATION_BASIC_INSTANCE_EVENTS = "application-basic-instance-events";

    @Input(KafkaChannels.AVERAGE_EVENT_INPUT)
    SubscribableChannel averageEventInput();

    @Output(KafkaChannels.APPLICATION_BASIC_INSTANCE_EVENTS)
    MessageChannel applicationBasicInstanceEvents();

}
