package io.netiot.applicationinterface.controllers;

import io.netiot.applicationinterface.models.ConfigurationModel;
import io.netiot.applicationinterface.services.ApplicationInstanceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/configuration")
@RequiredArgsConstructor
public class ApplicationConfigurationController {

    private final ApplicationInstanceService applicationInstanceService;

    @PreAuthorize("hasAnyAuthority('ENTERPRISE_ADMIN', 'ENTERPRISE_USER','FREE_USER', 'PREMIUM_USER', 'PARTNER_ADMIN', 'PARTNER_USER')")
    @GetMapping
    public ResponseEntity getApplicationConfigurationJSON(
            @RequestParam(name = "applicationInstanceId", defaultValue = "-1") final Long applicationInstanceId) throws IOException {
        return ResponseEntity.ok(applicationInstanceService.getApplicationConfiguration(applicationInstanceId));
    }

    @PreAuthorize("hasAnyAuthority('ENTERPRISE_ADMIN', 'ENTERPRISE_USER','FREE_USER', 'PREMIUM_USER', 'PARTNER_ADMIN', 'PARTNER_USER')")
    @PostMapping("/{applicationInstanceId}")
    public ResponseEntity saveApplicationConfiguration(@PathVariable(name = "applicationInstanceId") final Long applicationInstanceId,
                                                       @RequestBody final ConfigurationModel configurationModel) {
        applicationInstanceService.saveApplicationConfiguration(applicationInstanceId, configurationModel);
        return ResponseEntity.ok().build();
    }
}
