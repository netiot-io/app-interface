package io.netiot.applicationinterface.controllers;

import io.netiot.applicationinterface.models.ApplicationInstanceModel;
import io.netiot.applicationinterface.models.DeviceDataModelList;
import io.netiot.applicationinterface.services.ApplicationInstanceService;
import io.netiot.applicationinterface.services.FileService;
import io.netiot.applicationinterface.validators.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/application-instances")
@RequiredArgsConstructor
public class ApplicationInstanceController {

    private final ValidatorUtil validatorUtil;
    private final ApplicationInstanceService applicationInstanceService;
    private final FileService fileService;

    @PostMapping("/internal")
    public ResponseEntity createApplicationInstance(@RequestBody @Valid final ApplicationInstanceModel applicationInstanceModel,
                                                    final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        applicationInstanceService.createApplicationInstance(applicationInstanceModel);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/internal")
    public ResponseEntity updateApplicationInstance(@RequestBody @Valid final ApplicationInstanceModel applicationInstanceModel,
                                                    final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        applicationInstanceService.updateApplicationInstance(applicationInstanceModel);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/internal")
    public ResponseEntity deleteApplicationInstance(@RequestParam final Long applicationInstanceId){
        applicationInstanceService.deleteApplicationInstance(applicationInstanceId);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER', 'ENTERPRISE_ADMIN', 'ENTERPRISE_USER')")
    @GetMapping("/{applicationInstanceId}/data")
    public ResponseEntity getData(@PathVariable(name = "applicationInstanceId") final Long applicationInstanceId,
                                   @RequestParam(name = "millis-from") final Long millisFrom,
                                   @RequestParam(name = "millisTo", required = false) final Long millisTo) {
        return ResponseEntity.ok(DeviceDataModelList.builder()
                .devicesData(applicationInstanceService.getData(applicationInstanceId, millisFrom, millisTo == null ? System.currentTimeMillis() : millisTo))
                .build());
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','FREE_USER', 'PREMIUM_USER', 'ENTERPRISE_ADMIN', 'ENTERPRISE_USER')")
    @GetMapping
    public ResponseEntity getAppUI() throws IOException {
        return ResponseEntity.ok(fileService.getUIJs());
    }

}