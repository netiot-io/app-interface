package io.netiot.applicationinterface.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConfigurationDataModel {


    //ToDo here each application will add its own properties(e.g., co2Limit)
    @NotNull
    private Integer sensorLimit;

    @NotNull
    private List<Map<String, Object>> sensors;

}
