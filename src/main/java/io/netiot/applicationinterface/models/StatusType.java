package io.netiot.applicationinterface.models;

public enum StatusType {
    ENABLE {
        @Override
        public Boolean getActivation() {
            return Boolean.TRUE;
        }
    },
    DISABLE{
        @Override
        public Boolean getActivation() {
            return Boolean.FALSE;
        }
    };

    public abstract Boolean getActivation();

}