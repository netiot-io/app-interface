package io.netiot.applicationinterface.models.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeviceRegistrationEvent {

    private Long applicationId;

    private String deviceId;

    private DeviceRegistrationType deviceRegistrationType;

}
