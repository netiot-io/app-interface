package io.netiot.applicationinterface.models.events;

public enum DeviceRegistrationType {
    REGISTER, UNREGISTER
}
