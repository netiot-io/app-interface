package io.netiot.applicationinterface.models.events;

import com.fasterxml.jackson.databind.JsonNode;

public enum FieldType {
    TEXT {
        @Override
        public boolean validateType(final JsonNode payloadField) {
            return payloadField.isTextual();
        }
    },
    NUMBER {
        @Override
        public boolean validateType(final JsonNode payloadField) {
            return payloadField.isNumber();
        }
    },
    BOOLEAN {
        @Override
        public boolean validateType(final JsonNode payloadField) {
            return payloadField.isBoolean();
        }
    };

    public abstract boolean validateType(final JsonNode payloadField);
}
