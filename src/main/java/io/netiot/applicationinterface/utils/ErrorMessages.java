package io.netiot.applicationinterface.utils;

public final class ErrorMessages {

    public static final String UNAUTHORIZED_ACTION_ERROR_CODE = "backend.app-basic.unauthorized.action";
    public static final String UNAUTHORIZED_ACTION_ROLE_ERROR_CODE = "backend.app-basic.unauthorized.action.role";

    public static final String APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE = "backend.app-basic.applicationInstance.not.exist";
    public static final String APPLICATION_INSTANCE_ALREADY_EXIST_ERROR_CODE = "backend.app-basic.applicationInstance.already.exist";

    public static final String APPLICATION_INSTANCE_MISSING_VISUALIZATION_ERROR_CODE = "backend.app.missing.visualization";
    public static final String APPLICATION_INSTANCE_MISSING_CONFIG_ERROR_CODE = "backend.app.missing.config";
    public static final String APPLICATION_INSTANCE_CONFIG_ERROR_CODE = "backend.app.config.error";

    public static final String INTERNAL_SERVER_ERROR_CODE = "backend.devices.internal.server.error";

}
