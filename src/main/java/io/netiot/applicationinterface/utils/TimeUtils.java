package io.netiot.applicationinterface.utils;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class TimeUtils {

    public LocalDateTime getCurrentTime(){
        return LocalDateTime.now();
    }

}
