package io.netiot.applicationinterface.validators;

import io.netiot.applicationinterface.entities.ApplicationInstance;
import io.netiot.applicationinterface.entities.Role;
import io.netiot.applicationinterface.exceptions.UnauthorizedOperationException;
import io.netiot.applicationinterface.services.ApplicationService;
import io.netiot.applicationinterface.utils.AuthenticatedUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static io.netiot.applicationinterface.utils.ErrorMessages.UNAUTHORIZED_ACTION_ERROR_CODE;

@Slf4j
@Component
@RequiredArgsConstructor
public class ApplicationInstanceValidator {

    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final ApplicationService applicationService;

    public void validateOnGet(final ApplicationInstance applicationInstance) {
        checkOperationPermission(applicationInstance);
    }

    public void validateOnSave(final ApplicationInstance applicationInstance) {
        checkOperationPermission(applicationInstance);
    }

    private void checkOperationPermission(final ApplicationInstance applicationInstance) {
        final Role userRole = authenticatedUserInfo.getRole();
        if(userRole != Role.ADMIN) {
            Long userId = authenticatedUserInfo.getId();
            if(!applicationService.checkPermission(userId, applicationInstance.getApplicationId())) {
                throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }

}
