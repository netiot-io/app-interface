package io.netiot.applicationinterface.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "DEVICE_CONFIG")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeviceConfig {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEVICE_CONFIG_GEN")
    @SequenceGenerator(name = "SEQ_DEVICE_CONFIG_GEN", sequenceName = "SEQ_DEVICE_CONFIG", allocationSize = 1)
    private Long id;

    @ManyToOne(cascade= CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "application_instance_id")
    private ApplicationInstance applicationInstance;

    private Long deviceId;

    private String sensors;

}
