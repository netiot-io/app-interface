package io.netiot.applicationinterface.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "APPLICATION_INSTANCES")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApplicationInstance {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_APPLICATION_INSTANCE_GEN")
    @SequenceGenerator(name = "SEQ_APPLICATION_INSTANCE_GEN", sequenceName = "SEQ_APPLICATION_INSTANCE", allocationSize = 1)
    private Long id;

    private Long applicationId;

    private String name;

    @Column(name = "configured")
    private Boolean isConfigured;

    private Boolean activated;

    private String config;

}
