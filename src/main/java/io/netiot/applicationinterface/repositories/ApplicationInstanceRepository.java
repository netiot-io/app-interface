package io.netiot.applicationinterface.repositories;

import io.netiot.applicationinterface.entities.ApplicationInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApplicationInstanceRepository extends JpaRepository<ApplicationInstance,Long> {

    Optional<ApplicationInstance> findByApplicationId(final Long applicationId);

    void deleteByApplicationId(final Long applicationId);

}