package io.netiot.applicationinterface.repositories;

import io.netiot.applicationinterface.entities.DeviceConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceConfigRepository extends JpaRepository<DeviceConfig, Long> {

    List<DeviceConfig> findByApplicationInstance_ApplicationId(final Long applicationInstanceId);

    void deleteByApplicationInstance_ApplicationId(final Long applicationInstanceId);

}

