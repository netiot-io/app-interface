package io.netiot.applicationinterface.services;

import feign.FeignException;
import io.netiot.applicationinterface.exceptions.InternalErrorException;
import io.netiot.applicationinterface.feign.ApplicationClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Marker;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static io.netiot.applicationinterface.utils.ErrorMessages.INTERNAL_SERVER_ERROR_CODE;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class ApplicationService {

    private final Integer HTTP_STATUS_NOT_FOUND = 404;

    private final ApplicationClient applicationClient;

    public Boolean checkPermission(Long userId, Long applicationId) {
        try {
            ResponseEntity<Boolean> resp = applicationClient.checkPermission(userId, applicationId);
            return resp.getBody();
        } catch (FeignException e) {

            if (e.status() == HTTP_STATUS_NOT_FOUND) {
                return false;
            }

            log.error("Internal Error", e);

            throw new InternalErrorException(INTERNAL_SERVER_ERROR_CODE);
        } catch (Exception e) {
            log.error("Internal Error", e);

            throw new InternalErrorException(INTERNAL_SERVER_ERROR_CODE);
        }
    }

}
