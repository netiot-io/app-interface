package io.netiot.applicationinterface.services;

import io.netiot.applicationinterface.exceptions.ApplicationInstanceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static io.netiot.applicationinterface.utils.ErrorMessages.APPLICATION_INSTANCE_MISSING_CONFIG_ERROR_CODE;
import static io.netiot.applicationinterface.utils.ErrorMessages.APPLICATION_INSTANCE_MISSING_VISUALIZATION_ERROR_CODE;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileService {

    private static final String JS_CODE_FILE_PATH = "classpath*:static/main.*.js";
    private static final String CONFIG_FILE = "static/config.json";

    private ResourceLoader resourceLoader;

    @Autowired
    public FileService (ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    String getConfigJsonFileContent() {
        return getFileContent(CONFIG_FILE, APPLICATION_INSTANCE_MISSING_CONFIG_ERROR_CODE);
    }

    public String getUIJs() throws IOException {
        Resource[] resources = ResourcePatternUtils.getResourcePatternResolver(resourceLoader).getResources(JS_CODE_FILE_PATH);

        if (resources.length > 0) {
            return addEnvVariables(new BufferedReader(new InputStreamReader(resources[0].getInputStream()))
                    .lines().collect(Collectors.joining("\n")));
        }
        throw new ApplicationInstanceException(APPLICATION_INSTANCE_MISSING_VISUALIZATION_ERROR_CODE);
    }

    private String getFileContent(final String path, final String error) {
        try {
            InputStream is = new ClassPathResource(path).getInputStream();
            return new BufferedReader(new InputStreamReader(is))
                    .lines().collect(Collectors.joining("\n"));
        } catch (IOException exception) {
            log.error("Error code: " + error, exception);
            throw new ApplicationInstanceException(error);
        }
    }

    private String addEnvVariables(String jsContent) {
        return jsContent.replace("UI_APP_PATH", System.getenv("UI_APP_PATH"))
                .replace("UI_APP_SERVER_BASE_PATH", System.getenv("UI_APP_SERVER_BASE_PATH"))
                .replace("UI_APP_SERVER_APP_DATA", System.getenv("UI_APP_SERVER_APP_DATA"))
                .replace("UI_APP_DATA_FREQUENCY", System.getenv("UI_APP_DATA_FREQUENCY"))
                .replace("UI_APP_DATA_INTERVAL", System.getenv("UI_APP_DATA_INTERVAL"));
    }


}
