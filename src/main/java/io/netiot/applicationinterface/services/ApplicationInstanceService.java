package io.netiot.applicationinterface.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import io.netiot.applicationinterface.entities.ApplicationInstance;
import io.netiot.applicationinterface.entities.DeviceConfig;
import io.netiot.applicationinterface.exceptions.ApplicationInstanceException;
import io.netiot.applicationinterface.feign.IOServerClient;
import io.netiot.applicationinterface.mappers.ApplicationInstanceMapper;
import io.netiot.applicationinterface.models.*;
import io.netiot.applicationinterface.models.events.ApplicationInstanceEvent;
import io.netiot.applicationinterface.repositories.ApplicationInstanceRepository;
import io.netiot.applicationinterface.repositories.DeviceConfigRepository;
import io.netiot.applicationinterface.utils.AuthenticatedUserInfo;
import io.netiot.applicationinterface.validators.ApplicationInstanceValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static io.netiot.applicationinterface.utils.ErrorMessages.APPLICATION_INSTANCE_ALREADY_EXIST_ERROR_CODE;
import static io.netiot.applicationinterface.utils.ErrorMessages.APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor
public class ApplicationInstanceService {


    private final ApplicationInstanceRepository applicationInstanceRepository;
    private final ApplicationInstanceValidator applicationInstanceValidator;
    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final DeviceConfigRepository deviceConfigRepository;
    private final KafkaMessageSenderService kafkaMessageSenderService;
    private final DevicesService devicesService;
    private final FileService fileService;
    private final IOServerClient ioServerClient;

    private final static String SENSOR_DELIMITER = ";";

    private static final Gson gson = new Gson();

    public void saveApplicationConfiguration(final Long applicationInstanceId, final ConfigurationModel configurationModel) {
        ApplicationInstance applicationInstance = applicationInstanceRepository.findByApplicationId(applicationInstanceId)
                .orElseThrow(() -> { throw new ApplicationInstanceException(); });
        applicationInstanceValidator.validateOnSave(applicationInstance);

        //ToDo after request validation, here will be implemented app business logic
        ConfigurationDataModel configurationDataModel = configurationModel.getData();
        final Integer sensorLimit = configurationDataModel.getSensorLimit();
        List<Map<String, Object>> sensorModels = configurationDataModel.getSensors();
        List<DeviceConfig> deviceConfigs = sensorModels.stream()
                .map(sensorModel -> DeviceConfig.builder()
                        .applicationInstance(applicationInstance)
                        .deviceId(extractDeviceId((String)sensorModel.get("device")))
                        .sensors(String.join(";", (List<String>)sensorModel.get(sensorModel.get("device"))))
                        .build()
                )
                .collect(Collectors.toList());

        deviceConfigRepository.deleteByApplicationInstance_ApplicationId(applicationInstanceId);
        deviceConfigRepository.saveAll(deviceConfigs);

        ApplicationConfigModel acm = gson.fromJson(applicationInstance.getConfig(), ApplicationConfigModel.class);
        acm.setSensorLimit(sensorLimit);
        applicationInstance.setConfig(gson.toJson(acm));
        applicationInstance.setIsConfigured(Boolean.TRUE);
        applicationInstanceRepository.save(applicationInstance);

        sendApplicationInstanceEvents(applicationInstanceId, deviceConfigs);
    }

    private Long extractDeviceId(final String string) {
        if (!string.contains("(") || !string.contains(")")) {
            throw new ApplicationInstanceException();
        }

        String deviceId = string.substring(string.lastIndexOf("(") + 1, string.lastIndexOf(")"));
        if (deviceId.isEmpty()) {
            throw new ApplicationInstanceException();
        }
        return Long.parseLong(deviceId);
    }

    private List<String> extractFromRoomPayloadFields(final DeviceConfig device){
        return Arrays.asList(device.getSensors().split(SENSOR_DELIMITER));
    }

    private void sendApplicationInstanceEvents(Long applicationInstanceId, List<DeviceConfig> deviceConfigs) {
        List<ApplicationInstanceEvent> applicationInstanceEvents = deviceConfigs.stream()
                .map(device -> ApplicationInstanceEvent.builder()
                        .deviceId(device.getDeviceId())
                        .id(applicationInstanceId)
                        .payloadFields(extractFromRoomPayloadFields(device))
                        .build())
                .collect(Collectors.toList());

        applicationInstanceEvents.forEach(applicationInstanceEvent -> {
            kafkaMessageSenderService.sendApplicationInstanceEvent(applicationInstanceEvent);
            log.info(String.format("ApplicationInstanceEvent send for application instance %s with deviceId %s",
                    applicationInstanceId, applicationInstanceEvent.getDeviceId()));
        });
    }


    public String getApplicationConfiguration(final Long applicationInstanceId) throws IOException {
        String configJson = fileService.getConfigJsonFileContent();
        Long userId = authenticatedUserInfo.getId();
        List<DeviceApplicationModel> deviceApplicationModels = devicesService.getUserDevices(userId);

        final ObjectMapper objectMapper = new ObjectMapper();

        JsonNode jsonNode = objectMapper.readTree(configJson);
        setSchemaJsonFields(jsonNode, deviceApplicationModels);

        Optional<ApplicationInstance> applicationInstance = applicationInstanceRepository.findByApplicationId(applicationInstanceId);

        if(applicationInstance.isPresent()) {
            applicationInstanceValidator.validateOnGet(applicationInstance.get());
            setDataJsonFields(applicationInstance.get(), objectMapper, jsonNode);
        }

        return jsonNode.toString();
    }

    private void setDataJsonFields(final ApplicationInstance applicationInstance, final ObjectMapper objectMapper, final JsonNode jsonNode) {
        if (applicationInstance.getIsConfigured()) {
            List<DeviceConfig> deviceConfigs = deviceConfigRepository.findByApplicationInstance_ApplicationId(applicationInstance.getApplicationId());
            JsonNode data = jsonNode.path("data");
            JsonNode sensorsField = data.path("sensors");

            ApplicationConfigModel acm = gson.fromJson(applicationInstance.getConfig(), ApplicationConfigModel.class);
            deviceConfigs.forEach(deviceConfig -> {
                ((ObjectNode)data).put("sensor_limit", acm.getSensorLimit());

                ObjectNode objectNode = objectMapper.createObjectNode();

                String deviceName = devicesService.getDeviceName(deviceConfig.getDeviceId());
                objectNode.put("device", deviceName + " (" + deviceConfig.getDeviceId() + ")");
                ArrayNode sensor = objectNode.putArray(deviceName + " (" + deviceConfig.getDeviceId() + ")");
                Arrays.stream(deviceConfig.getSensors().split(";")).forEach(sensor::add);

                ((ArrayNode) sensorsField).add(objectNode);
            });
        }
    }

    private void setSchemaJsonFields(final JsonNode jsonNode, final List<DeviceApplicationModel> deviceApplicationModels) {


        JsonNode enumDevices = jsonNode.path("schema").path("properties").path("sensors")
                .path("items").path("properties").path("device").path("enum");
        JsonNode fieldsPath = jsonNode.path("schema").path("properties").path("sensors")
                .path("items").path("properties");
        ArrayNode uiFieldsPath = (ArrayNode) jsonNode.path("uischema").path("elements");

        List<String> devicesNames = deviceApplicationModels.stream()
                .map(deviceApplicationModel -> deviceApplicationModel.getName() + " (" + deviceApplicationModel.getId() + ")")
                .collect(Collectors.toList());

        for (DeviceApplicationModel dam: deviceApplicationModels) {
            ObjectNode deviceFieldsNode = ((ObjectNode)fieldsPath).putObject(dam.getName() + " (" + dam.getId() + ")");
            deviceFieldsNode.put("type", "array");
            ObjectNode items = deviceFieldsNode.putObject("items");
            items.put("type", "string");
            ArrayNode fields = items.putArray("enum");
            configurationJsonAddValues(fields, new TreeSet<>(dam.getDecoderFields()));
        }

        Iterator<JsonNode> it = uiFieldsPath.elements();
        while (it.hasNext()) {
            JsonNode node = it.next();
            if (node.has("scope") && node.get("scope").asText().equals("#/properties/sensors")) {
                ArrayNode elements = (ArrayNode) node.path("options").path("detail").path("elements");
                final ObjectMapper objectMapper = new ObjectMapper();
                for (DeviceApplicationModel dam: deviceApplicationModels) {
                    ObjectNode objectNode = objectMapper.createObjectNode();
                    objectNode.put("type", "Control");
                    objectNode.put("label", "Sensors");
                    objectNode.put("scope", "#/properties/" + dam.getName() + " (" + dam.getId() + ")");
                    ObjectNode ruleNode = objectNode.putObject("rule");
                    ruleNode.put("effect", "SHOW");

                    ObjectNode conditionNode = ruleNode.putObject("condition");
                    conditionNode.put("scope", "#/properties/device");

                    ObjectNode schemaNode = conditionNode.putObject("schema");
                    ArrayNode enumArray = schemaNode.putArray("enum");

                    enumArray.add(dam.getName() + " (" + dam.getId() + ")");

                    elements.add(objectNode);
                }

                break;
            }
        }

        configurationJsonAddValues((ArrayNode) enumDevices, devicesNames);
    }

    private void configurationJsonAddValues(final ArrayNode configFieldJsonNode,
                                            final Collection<String> values) {
        configFieldJsonNode.removeAll();
        values.forEach(configFieldJsonNode::add);
    }

    public void createApplicationInstance(final ApplicationInstanceModel applicationInstanceModel) {
        Optional<ApplicationInstance> applicationInstanceOptional =
                applicationInstanceRepository.findByApplicationId(applicationInstanceModel.getId());

        if (applicationInstanceOptional.isPresent()) {
            log.error("Error code: " + APPLICATION_INSTANCE_ALREADY_EXIST_ERROR_CODE);
            throw new ApplicationInstanceException(APPLICATION_INSTANCE_ALREADY_EXIST_ERROR_CODE);
        }

        ApplicationInstance applicationInstance = ApplicationInstanceMapper.toEntity(applicationInstanceModel);
        applicationInstance.setIsConfigured(Boolean.FALSE);
        applicationInstanceRepository.save(applicationInstance);
    }

    public void updateApplicationInstance(final ApplicationInstanceModel applicationInstanceModel) {
        Optional<ApplicationInstance> applicationInstanceOptional =
                applicationInstanceRepository.findByApplicationId(applicationInstanceModel.getId());
        ApplicationInstance applicationInstance = applicationInstanceOptional
                .orElseThrow(() -> {
                    log.error("Error code: " + APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE);
                    throw new ApplicationInstanceException(APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE);
                });

        applicationInstance.setName(applicationInstanceModel.getName());
        applicationInstance.setActivated(applicationInstanceModel.getStatusType().getActivation());

        applicationInstanceRepository.save(applicationInstance);
    }

    public void deleteApplicationInstance(final Long applicationInstanceId) {
        Optional<ApplicationInstance> applicationInstanceOptional = applicationInstanceRepository.findByApplicationId(applicationInstanceId);
        applicationInstanceOptional.orElseThrow(() -> {
            log.error("Error code: " + APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE);
            throw new ApplicationInstanceException(APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE);
        });
        deviceConfigRepository.deleteByApplicationInstance_ApplicationId(applicationInstanceId);
        applicationInstanceRepository.deleteByApplicationId(applicationInstanceId);
    }

    public List<DeviceDataModel> getData(final Long applicationInstanceId, final Long millisFrom, final Long millisTo) {
        final ApplicationInstance applicationInstance = applicationInstanceRepository.findByApplicationId(applicationInstanceId)
                .orElseThrow(() -> { throw new ApplicationInstanceException(); });

        return deviceConfigRepository.findByApplicationInstance_ApplicationId(applicationInstance.getApplicationId())
                .stream()
                .map(deviceConfig -> DeviceDataModel.builder()
                        .samples(ioServerClient.getDeviceData(deviceConfig.getDeviceId(), millisFrom, millisTo))
                        .build())
                .collect(Collectors.toList());
    }

}
