package io.netiot.applicationinterface.services;

import io.netiot.applicationinterface.configurations.KafkaChannels;
import io.netiot.applicationinterface.models.events.ApplicationInstanceEvent;
import io.netiot.applicationinterface.models.events.DeviceRegistrationEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaMessageSenderService {

    private final KafkaChannels kafkaChannels;

    private void sendMessage(final MessageChannel channel, final Message message) {
        try {
            final boolean messageSent = channel.send(message);
            if (!messageSent) {
                log.error("The message could not be sent due to a non-fatal reason: {}", message.getPayload());
            } else {
                log.info("Event sent: {}", message.getPayload());
            }
        } catch (RuntimeException ex) {
            log.error("Non-recoverable error. Unable to send message='{}'", message.getPayload(), ex);
        }

    }

    public void sendApplicationInstanceEvent(final ApplicationInstanceEvent applicationInstanceEvent) {
        Message message = MessageBuilder.withPayload(applicationInstanceEvent)
                .setHeader(KafkaHeaders.MESSAGE_KEY, applicationInstanceEvent.getDeviceId().toString().getBytes())
                .build();
        sendMessage(kafkaChannels.applicationBasicInstanceEvents(), message);
    }

}
