package io.netiot.applicationinterface.services;

import io.netiot.applicationinterface.feign.DevicesClient;
import io.netiot.applicationinterface.models.DeviceApplicationModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DevicesService {

    private final DevicesClient devicesClient;

    List<DeviceApplicationModel> getUserDevices(final Long userId){
        return devicesClient.getUserDevices(userId);
    }

    String getDeviceName(final Long deviceId){
        return devicesClient.getDeviceName(deviceId);
    }

}
