package io.netiot.applicationinterface.mappers;

import io.netiot.applicationinterface.entities.ApplicationInstance;
import io.netiot.applicationinterface.models.ApplicationInstanceModel;

public final class ApplicationInstanceMapper {

    private ApplicationInstanceMapper() {
    }

    public static ApplicationInstance toEntity(final ApplicationInstanceModel applicationInstanceModel) {
        return ApplicationInstance.builder()
                .applicationId(applicationInstanceModel.getId())
                .name(applicationInstanceModel.getName())
                .activated(applicationInstanceModel.getStatusType().getActivation())
                .config(applicationInstanceModel.getConfig() == null ? "{}" : applicationInstanceModel.getConfig())
                .build();
    }

}
