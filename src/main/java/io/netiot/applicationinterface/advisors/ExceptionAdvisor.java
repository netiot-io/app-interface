package io.netiot.applicationinterface.advisors;

import io.netiot.applicationinterface.exceptions.ApplicationInstanceException;
import io.netiot.applicationinterface.exceptions.BindingResultException;
import io.netiot.applicationinterface.models.ErrorModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public final class ExceptionAdvisor {

    @ExceptionHandler(value = {BindingResultException.class})
    public ResponseEntity errorHandle(final BindingResultException bindingResultRuntimeException) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorModel
                        .builder()
                        .error(bindingResultRuntimeException.getErrors())
                        .build());
    }

    @ExceptionHandler(value = {ApplicationInstanceException.class})
    public ResponseEntity errorHandle(final ApplicationInstanceException applicationInstanceException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", applicationInstanceException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

}