package io.netiot.applicationinterface.feign;

import io.netiot.applicationinterface.models.DeviceApplicationModel;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "devices")
public interface DevicesClient {

    @RequestMapping(method = RequestMethod.GET, value = "V1/devices/internal")
    List<DeviceApplicationModel> getUserDevices(@RequestParam(name = "userId") final Long userId);

    @RequestMapping(method = RequestMethod.GET, value = "V1/devices/internal/{deviceId}")
    String getDeviceName(@PathVariable(name = "deviceId") final Long deviceId);

}
