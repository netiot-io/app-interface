package io.netiot.applicationinterface.feign;

import io.netiot.applicationinterface.models.DataSampleModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "io-server", fallback = IOServerClientFallback.class)
public interface IOServerClient {

    @RequestMapping(method = RequestMethod.GET, value = "V1/data/device-data")
    List<DataSampleModel> getDeviceData(@RequestParam(name = "deviceId") final Long deviceId,
                                        @RequestParam(name = "millisFrom") final Long millisFrom,
                                        @RequestParam(name = "millisTo") final Long millisTo);

}
