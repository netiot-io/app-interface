package io.netiot.applicationinterface.feign;

import io.netiot.applicationinterface.models.DataSampleModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class IOServerClientFallback implements IOServerClient {

    @Override
    public List<DataSampleModel> getDeviceData(final Long deviceId, final Long millisFrom, final Long millisTo) {
        return new ArrayList<>();
    }

}
