package io.netiot.applicationinterface.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "application-registry")
public interface ApplicationClient {

    @RequestMapping(method = RequestMethod.GET, value = "V1/application-instance/internal/checkPermission")
    ResponseEntity<Boolean> checkPermission(@RequestParam final Long userId, @RequestParam final Long applicationId);

}
