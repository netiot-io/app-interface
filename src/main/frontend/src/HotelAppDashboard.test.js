import React from 'react';
import { shallow } from 'enzyme';
import HotelAppDashboard from './HotelAppDashboard';

const appConfig = {
  appId: 1,
  data: {
    co2_limit: 100,
    sensors: []
  }
};

it('renders without crashing', () => {
  const wrapper = shallow(<HotelAppDashboard appConfig={appConfig} />);
});
