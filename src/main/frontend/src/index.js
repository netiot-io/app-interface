import React from 'react';
import ReactDOM from 'react-dom';
import HotelAppDashboard from './HotelAppDashboard';

// ia scriptul care incarca vizualizarea. Fiecare app are propriul id pe care il stie si UI-ul
const myScript = document.getElementById('tempApp');
// afiseaza dashboardul aplicatiei, folosind parametrii definiti in UI
ReactDOM.render(<HotelAppDashboard appConfig={JSON.parse(myScript.getAttribute('appConfig').replace(/'/g, '"'))} />,
  document.getElementById(myScript.getAttribute('div-id')));