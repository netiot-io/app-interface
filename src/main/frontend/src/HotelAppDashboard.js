import React, {Component, Fragment} from 'react';
import Chart from './components/Chart';
import {authHeader} from './utils/auth-header';
import moment from 'moment';
import MyGauge from "./components/MyGauge";

const divStyle = {
  display: 'flex',
  flexWrap: 'wrap'
};

const lineStyle = {
  width: '100vw'
};

class HotelAppDashboard extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      appId: props.appConfig.appId,
      sensorLimit: props.appConfig.data.sensor_limit,
      devicesConfig: props.appConfig.data.sensors,
      devicesData: []
    };
    this.startTime = moment().valueOf() - parseInt(process.env.REACT_APP_DATA_INTERVAL, 10);
    this.lastValue = {};
  };

  componentDidMount() {
      const intervalId = setInterval(this.update.bind(this), process.env.REACT_APP_DATA_FREQUENCY);
      this.setState({intervalId: intervalId});
  };

  componentWillUnmount() {
      clearInterval(this.state.intervalId);
  };

  update() {
      /*
      * TEST CODE FOR VISUALIZATION
      * Uncomment this code and comment the rest of the function. Adapt the data according to your app.
      */
      // this.setState({
      //     devicesData: [{
      //         samples: [
      //             {deviceId: 23, timestamp: 1556869576958, timestamp_r: null, values: '[ {"co2": { "value": 405 }} ]'},
      //             {deviceId: 23, timestamp: 1556869576958, timestamp_r: null, values: '[ {"temperature": { "value": 40 }} ]'}
      //         ]
      //     }]
      // });

      const requestOptions = {
          method: 'GET',
          headers: authHeader()
      };
      fetch(`${process.env.REACT_APP_SERVER_BASE_PATH}${process.env.REACT_APP_PATH}${process.env.REACT_APP_SERVER_APP_DATA}${this.state.appId}/data?millis-from=${this.startTime}`, requestOptions)
          .then(response => response.json())
          .then(data => {
              this.setState({ devicesData: data.devices_data });
              for (let deviceData of data.devices_data) {
                  for (let sample of deviceData.samples) {
                      if (sample.timestamp > this.startTime) {
                          this.startTime = sample.timestamp;
                      }
                  }
              }
          })
          .catch(e => console.log(e));
  };

  extractDeviceId(text) {
      return parseInt(text.substring(text.lastIndexOf("(") + 1, text.lastIndexOf(")")), 10);
  }

  render() {
    return (<div style={divStyle}>
              <div style={lineStyle}>
                  {
                      this.state.devicesConfig.map(deviceConfig => {
                          let samples = [];
                          this.state.devicesData.forEach(deviceData => samples.push(...deviceData.samples));
                          const data = {};
                          const pointBackgroundColors = {};
                          const deviceSensors = deviceConfig[deviceConfig.device];
                          samples.filter(sample => sample.deviceId === this.extractDeviceId(deviceConfig.device))
                              .filter(sample => {
                                  const values = JSON.parse(sample.values);
                                  const sensorValues = values.filter(value => deviceSensors.includes(Object.keys(value)[0]));
                                  return sensorValues.length > 0;
                              })
                              .forEach(sample => {
                                  JSON.parse(sample.values).filter(value => deviceSensors.includes(Object.keys(value)[0])).forEach(value => {
                                      const item = {
                                          x: sample.timestamp,
                                          y: value[Object.keys(value)[0]].value
                                      };
                                      data[Object.keys(value)[0]] = Object.keys(value)[0] in data ? [...data[Object.keys(value)[0]], item] : [item];
                                      const color = value[Object.keys(value)[0]].value > this.state.sensorLimit ? '#f44842' : '#a0f441';
                                      pointBackgroundColors[Object.keys(value)[0]] = Object.keys(value)[0] in pointBackgroundColors ?
                                          [...pointBackgroundColors[Object.keys(value)[0]], color] :
                                          [color];
                                  });
                              });
                          if (Object.keys(data).length > 0) {
                              Object.keys(data).forEach(sensor => {
                                  this.lastValue[deviceConfig.device] = { ...this.lastValue[deviceConfig.device], [sensor]: data[sensor][data[sensor].length-1].y };
                              });
                          }
                          return (<div>
                              {deviceSensors.map(deviceSensor => {
                                return (
                                    <Fragment>
                                        <div style={{margin: '0 auto', width: '400px'}} >
                                           <MyGauge value={deviceConfig.device in this.lastValue ?
                                               (deviceSensor in this.lastValue[deviceConfig.device] ? this.lastValue[deviceConfig.device][deviceSensor] : 0)
                                               : 0} />
                                        </div>
                                        <Chart
                                            device={deviceConfig}
                                            sensorType={deviceSensor}
                                            data={deviceSensor in data? data[deviceSensor] : {}}
                                            pointColor={deviceSensor in pointBackgroundColors ? pointBackgroundColors[deviceSensor] : {}}/>
                                    </Fragment>
                                );
                              })}
                          </div>);
                      })
                  }
              </div>
            </div>);
  }
}

export default HotelAppDashboard;
