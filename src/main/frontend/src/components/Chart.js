import React, { Component } from 'react';
import {Line} from 'react-chartjs-2';

class Chart extends Component {
  constructor(props, context) {
    super(props, context);
    let um = '';
    if (props.sensorType === 'co2') {
        um = 'ppm';
    } else if (props.sensorType === 'light') {
        um = 'lm';
    } else if (props.sensorType === 'humidity') {
        um = '%';
    } else if (props.sensorType === 'pressure') {
        um = 'Pa';
    } else if (props.sensorType === 'temperature') {
        um = '°C';
    }

    const datasets = [{
      label: `${props.device.device} - ${props.sensorType.toUpperCase()} ( ${um} )`,
      fill: false,
      lineTension: 0.1,
      backgroundColor: '#418bf4',
      borderColor: '#418bf4',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: props.pointColor,
      pointBackgroundColor: props.pointColor,
      pointBorderWidth: 1,
      pointHoverRadius: 6,
      pointHoverBackgroundColor: props.pointColor,
      pointHoverBorderColor: props.pointColor,
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: props.data,
    }];

    this.state = {
      width: 400,
      height: 300,
      device: props.device,
      data: {
        datasets: datasets
      }
    };
  };

  componentWillReceiveProps = nextProps => {
    const data = this.state.data;
    data.datasets = [{
        ...data.datasets[0],
        data: [...data.datasets[0].data, ...nextProps.data],
        pointBorderColor: [...data.datasets[0].pointBorderColor, ...nextProps.pointColor],
        pointBackgroundColor: [...data.datasets[0].pointBackgroundColor, ...nextProps.pointColor],
        pointHoverBackgroundColor: [...data.datasets[0].pointHoverBackgroundColor, ...nextProps.pointColor],
        pointHoverBorderColor: [...data.datasets[0].pointHoverBorderColor, ...nextProps.pointColor]
    }];
    this.setState({data});
  };

  render() {
    return (<div style={{maxHeight: this.state.height}}>
        <Line
              data={this.state.data} width={this.state.width}
              height={this.state.height}
              options={{
                 maintainAspectRatio: false,
                 scales: {
                  xAxes: [{
                    type: 'time',
                    time: {
                        displayFormats: {
                            millisecond:  'll h:mm:ss a',
                            second: 'll h:mm:ss a',
                            minute: 'll h:mm:ss a',
                            hour: 'll h:mm:ss a',
                            day: 'll h:mm:ss a',
                            week: 'll h:mm:ss a',
                            month: 'll h:mm:ss a',
                            quarter: 'll h:mm:ss a',
                            year: 'll h:mm:ss a',
                        }
                    },
                    ticks: {
                      source: 'data',
                      autoSkip: true
                    }
                  }]
                },
                animation: {
                  easing: "easeInOutBack"
                }
              }} /></div>
            );
  };
}

export default Chart;
