import React, { Component } from 'react';
import Gauge from 'react-svg-gauge';

class MyGauge extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            width: 400,
            height: 300,
            value: 0
        };
    };

    render() {
        return (
            <Gauge value={this.props.value} width={this.state.width} height={this.state.height} label='' max={1000}/>
        );
    }
}

export default MyGauge;
