import React from 'react';
import { shallow } from 'enzyme';
import Chart from './Chart';

const room = {
  id: 1,
  timestamp: 1555677020878,
  timestampR: 1555677020878,
  values: "[{\"co2\":{\"value\":731}}]"
};


it('renders without crashing', () => {
  const wrapper = shallow(<Chart room={room} data={[]} pointColor={[]} />);
});
